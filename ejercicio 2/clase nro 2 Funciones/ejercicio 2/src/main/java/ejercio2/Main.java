import javax.swing.JOptionPane;
package ejercio2;
public class Main {
    public static void main(String[] args) {
        int MAX;
        MAX = Integer.parseInt(JOptionPane.showInputDialog(null,"INGRESE EL TAMA�O DE LA COLA"));   
        Cola = new ColaArray(MAX);
        MenuCola();
    }
    public static void MenuCola()
    {
        int Opcion;     
        do
        {
            Opcion = Integer.parseInt(JOptionPane.showInputDialog(null,
                    "1. INGRESAR DATOS\n"+
                    "2. ELIMINAR DATOS\n"+
                    "3. OBSERVAR DATOS\n"+
                    "4. VACIAR COLA\n"+
                    "5. SUMAR ELEMENTOS DE LA COLA\n"+
                    "6. SALIR\n"+
                    "--------------------------------------------------------\n"+
                    "INGRESE LA OPCION [1 - 6]","MENU COLA",JOptionPane.QUESTION_MESSAGE));
            
            switch(Opcion)
            {
                case 1: Cola.InsertarCola();break;
                case 2: Cola.EliminarCola();break;
                case 3: Cola.MostrarCola();break;
                case 4: Cola.VaciarCola();break;
                case 5: Cola.sum();break;
                case 6: System.exit(0);break;
                default: JOptionPane.showMessageDialog(null,"INGRESE UNA OPCION VALIDA \n","ERROR OPCION",JOptionPane.WARNING_MESSAGE);break;
            }
        }
        while(true); 
    }
}
